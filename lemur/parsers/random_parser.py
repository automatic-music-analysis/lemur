"""Baseline parser which constructs a random tree using COSIATEC patterns."""
from pathlib import Path
from lemur.omnisia.parsetecs import get_tecs
from midi_point_set import get_tick_point_set
from lemur.viewer.tree_viewer import TreeNode
import random


def get_tree(
        midi: Path,
        ) -> TreeNode:
    """Construct a random tree merging COSIATEC patterns randomly.

    :param midi: Input MIDI file to analyze.
    """
    # Get OMNISIA patterns
    tecs = get_tecs(midi)

    # Create random tree
    # First create patterns
    patterns = dict()
    for i, tec in enumerate(tecs):
        points = list()
        points.extend(tec.pattern.raw_pattern)
        for (x, y) in tec.translations:
            new_pattern = tuple(
                (xr+x, yr+y)
                for xr, yr in tec.pattern.raw_pattern
            )
            points.extend(new_pattern)
        name = f"P{i}"
        patterns[name] = points

    # Then create random tree
    nodes = [
        TreeNode(name=name, children=[pattern])
        for name, pattern in patterns.items()
    ]
    while len(nodes) > 1:
        # Merge nodes randomly
        idxs = list(range(len(nodes)))
        i1, i2 = random.sample(idxs, k=2)
        n1 = nodes[i1]
        n2 = nodes[i2]
        new_node = TreeNode(
            name=f"({n1.name}, {n2.name})",
            children=[n1, n2],
        )
        other_nodes = [nodes[i] for i in idxs if i not in [i1, i2]]
        nodes = [new_node, *other_nodes]
    return nodes[0]


if __name__ == "__main__":
    # Debugging
    from lemur.viewer.tree_viewer import open_viewer
    import argparse
    parser = argparse.ArgumentParser(
        description='Visualize interactively the random parser output on a MIDI file.',
    )
    parser.add_argument(
        "--input-midi",
        help="MIDI file that will be converted.",
        required=True,
        type=Path,
    )
    args = parser.parse_args()
    tree = get_tree(args.input_midi)
    point_set = get_tick_point_set(args.input_midi)
    open_viewer(point_set, tree)
