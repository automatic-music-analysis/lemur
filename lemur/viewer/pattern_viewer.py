"""Visualize point sets and patterns."""
from pathlib import Path
from lemur.omnisia.parsetecs import get_tecs
from midi_point_set import get_tick_point_set
from midi_point_set import Tick
from midi_point_set import Pitch
from bokeh.plotting import figure, show
from bokeh.models import HoverTool
from bokeh.models import MultiChoice
from bokeh.layouts import column
from bokeh.models import ColumnDataSource
from bokeh.models.callbacks import CustomJS

PointSet = list[tuple[Tick, Pitch]]


def open_viewer(
        point_set: PointSet,
        patterns: dict[str, PointSet],
        ):
    """Open LEMUR's interactive pattern viewer.

    :param point_set: Base point set.
    :param patterns: Patterns to visualize on top of the MIDI.
    """
    # Create dropdown menu for patterns
    multi_choice = MultiChoice(value=[], options=list(patterns.keys()))

    # Create hover tooltips
    hover = HoverTool(tooltips=[
        ("(x,y)", "(@x, @y)"),
    ])

    # Create figure with list of tools
    p = figure(tools=['pan, ywheel_zoom, xwheel_zoom, lasso_select', hover])

    # Plot points
    x = [x for x, _ in point_set]
    y = [y for _, y in point_set]
    source = ColumnDataSource(
        data=dict(
            x=x, y=y, color=["gray" for _ in x],
        )
    )
    p.circle(x="x", y="y", source=source, color="color")

    # Reactive multi choice
    multi_choice_args = dict(
        source=source,
        point_set=point_set,
        patterns=patterns,
    )
    multi_choice.js_on_change('value', CustomJS(args=multi_choice_args, code="""
        // Create new data
        const xs = [];
        const ys = [];
        const colors = [];

        // Keep track of points already drawn
        const used_keys = new Set();

        // Plot selected patterns
        const selected_patterns = this.value;
        for (const pattern_name of selected_patterns) {
            for (const [x, y] of patterns[pattern_name]) {
                const key = JSON.stringify([x, y]);
                used_keys.add(key);
                xs.push(x);
                ys.push(y);
                colors.push("blue");
            }
        }

        // Plot base MIDI
        for (const [x, y] of point_set) {
            // If point has already been drawn, ignore
            const key = JSON.stringify([x, y]);
            if (used_keys.has(key))
                continue;
            xs.push(x);
            ys.push(y);
            colors.push("gray");
        }

        // Update data
        source.data = {x: xs, y: ys, color: colors};
    """))

    layout = column(multi_choice, p)
    show(layout)


if __name__ == "__main__":
    # Debugging
    midi = Path("../K545-1 - exposition.mid")

    # Get OMNISIA patterns
    tecs = get_tecs(midi)

    # Get raw point set
    point_set = get_tick_point_set(midi)

    # Parse tecs into patterns
    patterns = dict()
    for i, tec in enumerate(tecs):
        points = list()
        points.extend(tec.pattern.raw_pattern)
        for (x, y) in tec.translations:
            new_pattern = tuple(
                (xr+x, yr+y)
                for xr, yr in tec.pattern.raw_pattern
            )
            points.extend(new_pattern)
        name = f"P{i}"
        patterns[name] = points
    open_viewer(point_set, patterns)
