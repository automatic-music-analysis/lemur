"""
parsetecs.py

Script to read outputs of point-set compression algorithms such as COSIATEC (see Meredith, 2016).
Represents Maximal Translatable Pattern (MTP) Translation Equivalence Classes (TEC).

5/1/2023
Sean Anderson
"""
import re
from pathlib import Path
from lemur.omnisia.omnisia import get_omnisia_output
from lemur.omnisia.omnisia import default_omnisia_path
from dataclasses import dataclass


@dataclass(eq=True, frozen=True)
class Pattern:
    raw_pattern: tuple[tuple[int, int], ...]

    @property
    def normalized_pattern(self) -> tuple[tuple[int, int], ...]:
        """
        REQUIRES:
        EFFECTS:  Shifts all tatums (temporal onsets) in pattern to start at 0
        :param self:
        :param pattern:
        :return:
        """
        min_tatum = min([ p[0] for p in self.raw_pattern ])
        normalized = tuple( (p[0] - min_tatum, p[1]) for p in self.raw_pattern )
        return normalized


@dataclass(eq=True, frozen=True)
class Tec:
    pattern: Pattern
    translations: tuple[tuple[int, int], ...]


def parse_tec(line: str) -> Tec:
    """
    REQUIRES: line is one tec line in .cos log output of COSIATEC (OMNISIA implementation)
              expected format:
                "T(P(p(x1,y1),p(x2,y2),...),V(v(t1,p1),v(t2,p2),...))"
                where x#,y# are pitch-onset coordinates, and t#,p# are translation in tatums, pitch
    :param line:
    :return:
    """
    # get base pattern
    point_regex = re.compile(r'p\((\d+),(\d+)\)')
    pattern_points_str = point_regex.findall(line)
    pattern_points = tuple(
        (int(p[0]), int(p[1]))
        for p in pattern_points_str
    )

    # get vector translations
    translation_regex = re.compile(r'v\((\d+),(-?\d+)\)')
    translation_vectors_str = translation_regex.findall(line)
    translations = tuple(
        (int(v[0]), int(v[1]))
        for v in translation_vectors_str
    )

    tec = Tec(Pattern(pattern_points), translations)
    return tec


def get_tecs(
        midi: Path,
        omnisia_jar_path: Path=default_omnisia_path,
        ) -> set[Tec]:
    """Return OMNISIA's output TECs for the given MIDI.

    :param midi: Input MIDI file path.
    :param default_omnisia_url: URL to the OMNISIA JAR.
    :param kwargs: Parameters to OMNISIA.
    """
    # Get OMNISIA's output
    output = get_omnisia_output(midi, omnisia_jar_path)

    # Extract Tec strings
    # HACK ALERT: read lines until the first empty line
    # (not much we can do other than hack without official description of
    # output)
    lines = output.split("\n")
    tec_lines = list()
    for line in lines:
        l = line.strip()
        if len(l) == 0:
            break
        tec_lines.append(l)

    # Parse each Tec string into a Tec
    alltecs = { parse_tec(line) for line in tec_lines }
    return alltecs
