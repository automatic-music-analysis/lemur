# LEMUR

(Layered Expressive Musical Unified Representations)

## Development instructions

1. Clone repository recursively with submodules.

2. Install package (e.g., `poetry install`, `pip install -e .`).

3. Make sure you have the Java runtime installed (i.e., `$ java` is executable).

3. Everything should be set to start hacking.

## Project structure

```
lemur
|- omnisia        Interface with OMNISIA
|- viewer         Utilities for visualization
|- parsers        Algorithms for constructing structured representations of music
```
