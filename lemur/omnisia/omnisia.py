"""Entry-point for calling OMNISIA."""
from pathlib import Path
import subprocess
import tempfile


default_omnisia_path = Path(__file__).parent/"omnisia"/"Points"/"jars"/"omnisia20190506f4c93b2.jar"


def get_omnisia_output(
        midi: Path,
        omnisia_jar_path: Path,
        ) -> str:
    """Call OMNISIA on the given MIDI file and return its raw output as a
    string.

    :param midi: Input MIDI file path.
    :param default_omnisia_url: URL to the OMNISIA JAR.
    :param kwargs: Parameters to OMNISIA.
    """
    # Create temporary directory for OMNISIA
    with tempfile.TemporaryDirectory() as tdir:
        # TODO: support passing arguments to OMNISIA
        command = [
            "java",
            "-jar",
            str(omnisia_jar_path),
            "-i",
            str(midi),
            "-o",
            tdir,
        ]

        # Execute the JAR file
        try:
            subprocess.run(command, check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as e:
            print(e.stdout)
            print(e.stderr)
            raise e

        # Identify output file
        cos_files = list(Path(tdir).glob("**/*.cos"))
        assert len(cos_files) == 1, "OMNISIA should have a single output .cos"
        cos_file = cos_files[0]

        # Read output file
        with open(cos_file, "rt") as fp:
            output = fp.read()
    return output
