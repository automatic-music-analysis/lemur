"""Parser that uses handcrafted heuristics to construct a hierarchy using
local information of the nodes."""
from pathlib import Path

import midi_point_set
from lemur.omnisia.parsetecs import get_tecs
from midi_point_set import get_tick_point_set
from lemur.viewer.tree_viewer import TreeNode
from lemur.viewer.tree_viewer import get_points
from lemur.viewer.tree_viewer import PointSet
from midi_point_set import Tick
from midi_point_set import Pitch
import itertools


def get_time_overlap(p1: PointSet, p2: PointSet) -> int:
    """Get the overlap in time between the two point sets.
    To each point set assign a line segment from the time of the first note to
    the time of the last note. This function returns the size of the
    intersection between the two segments.
    """
    # Get segment endpoints
    min1 = min(x for x, _ in p1)
    max1 = max(x for x, _ in p1)
    min2 = min(x for x, _ in p2)
    max2 = max(x for x, _ in p2)

    # Canonical order: segment 1 starts first or at the same time
    if min1 > min2:
        temp = min2, max2
        min2, max2 = min1, max1
        min1, max1 = temp

    # Three cases
    # Case 1:
    # S1:  |--------|
    # S2:             |---------|
    #
    # Case 2:
    # S1:  |--------|
    # S2:     |--|
    #
    # Case 3:
    # S1:  |--------|
    # S2:     |--------|
    if min2 > max1:
        return 0
    elif max2 <= max1:
        return max2-min2
    else:
        return max1-min2


def get_time_distance(p1: PointSet, p2: PointSet) -> int:
    """Get the distance in time between the two points sets, defined as
    the smallest distance between every pair of points in the x-axis.
    """
    distances = (
        abs(x1-x2)
        for (x1, _), (x2, _) in itertools.product(p1, p2)
    )
    return min(distances)



def get_merge_score(
        n1: TreeNode,
        n2: TreeNode,
        time_range: int,
        global_size: int,
        ) -> float:
    """Score the two nodes for merging.

    Note that this heuristic is local because it only uses the candidate
    nodes.

    This heuristic seeks to merge patterns that:
    - are short
    - have a lot of overlap in time
    - if none have overlap, then merge patterns that are contiguous
    """
    p1 = get_points(n1)
    p2 = get_points(n2)
    length = len(set(p1+p2))
    length_score = -length/global_size
    overlap = get_time_overlap(p1, p2)/time_range
    time_distance = get_time_distance(p1, p2)/time_range
    time_distance_score = -time_distance
    return 0.5*length_score + 0.3*overlap + 0.2*time_distance_score


def get_tree(
        midi: Path,
        ) -> TreeNode:
    """Construct a tree using handcrafted heuristics to construct a hierarchy,
    leveraging COSIATEC patterns to initialize nodes that get merged using
    local information of each node, like whether nodes overlap each other.

    :param midi: Input MIDI file to analyze.
    """
    # Get OMNISIA patterns
    tecs = get_tecs(midi)

    # Create initial nodes
    nodes = list()
    for i, tec in enumerate(tecs):
        points = list()
        for j, (x, y) in enumerate(tec.translations):
            points = list(
                (Tick(xr+x), Pitch(yr+y))
                for xr, yr in tec.pattern.raw_pattern
            )
            node = TreeNode(name=f"P_{i}_{j}", children=[points])
            nodes.append(node)

    # Merge nodes until a single root exists
    point_set = midi_point_set.get_tick_point_set(midi)
    T = max(x for x, _ in point_set)
    while len(nodes) > 1:
        # Sort pairs of nodes by score
        idxs = list(range(len(nodes)))
        pairs = [
            (i, j)
            for i in idxs
            for j in idxs
            if j > i
        ]

        # Greedily select the highest scoring pair
        i, j = max(
            pairs,
            key=lambda p: get_merge_score(
                nodes[p[0]], nodes[p[1]],
                time_range=T,
                global_size=len(point_set),
            )
        )
        n1, n2 = nodes[i], nodes[j]

        # Merge the two nodes
        new_node = TreeNode(f"({n1.name}, {n2.name})", children=[n1, n2])

        # Update nodes
        other_nodes = [nodes[k] for k in idxs if k not in [i, j]]
        nodes = [new_node, *other_nodes]

    return nodes[0]


if __name__ == "__main__":
    # Debugging
    from lemur.viewer.tree_viewer import open_viewer
    import argparse
    parser = argparse.ArgumentParser(
        description='Visualize interactively the local handcrafted parser output on a MIDI file.',
    )
    parser.add_argument(
        "--input-midi",
        help="MIDI file that will be converted.",
        required=True,
        type=Path,
    )
    args = parser.parse_args()
    tree = get_tree(args.input_midi)
    point_set = get_tick_point_set(args.input_midi)
    open_viewer(point_set, tree)
