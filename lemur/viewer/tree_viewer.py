"""Visualize hierarchical point sets."""
from midi_point_set import Tick
from midi_point_set import Pitch
from bokeh.plotting import figure, show
from bokeh.models import HoverTool
from bokeh.models import Select
from bokeh.layouts import column
from bokeh.models import ColumnDataSource
from bokeh.models.callbacks import CustomJS
from dataclasses import dataclass
from typing import Union
import bokeh.palettes
import json
from bokeh.models import Div


PointSet = list[tuple[Tick, Pitch]]


@dataclass
class TreeNode:
    name: str
    children: list[Union["TreeNode", PointSet]]


def get_points(n: TreeNode) -> PointSet:
    """Recursively get all the points in the given tree."""
    points = list()
    for child in n.children:
        if isinstance(child, TreeNode):
            points.extend(get_points(child))
        else:
            points.extend(child)
    return points


def open_viewer(
        point_set: PointSet,
        tree: TreeNode,
        ):
    """Open LEMUR's interactive hierarchical viewer.

    :param point_set: Base point set.
    :param node_decomposition: Mapping from node names to the children as lists of notes.
    """
    # Flatten tree
    def get_nodes(n: TreeNode) -> list[TreeNode]:
        children = list()
        for child in n.children:
            if isinstance(child, TreeNode):
                children.extend(get_nodes(child))
        return [n, *children]
    nodes = get_nodes(tree)

    # Convert nodes to node decompositions

    node_decompositions = dict[str, list[PointSet]]()
    for node in nodes:
        points = list()
        for child in node.children:
            if isinstance(child, TreeNode):
                points.append(get_points(child))
            else:
                points.append(child)
        node_decompositions[node.name] = points

    # Create dropdown menu for patterns
    multi_choice = Select(title="Node", options=list(node_decompositions.keys()))

    # Create hover tooltips
    hover = HoverTool(tooltips=[
        ("(x,y)", "(@x, @y)"),
    ])

    # Create figure with list of tools
    p = figure(tools=['pan, ywheel_zoom, xwheel_zoom, lasso_select', hover])

    # Plot points
    x = [x for x, _ in point_set]
    y = [y for _, y in point_set]
    source = ColumnDataSource(
        data=dict(
            x=x, y=y, color=["gray" for _ in x], alpha=[0.25 for _ in x],
        )
    )
    p.circle(x="x", y="y", source=source, color="color", alpha="alpha")

    # List of colors to choose from
    colors = list()
    for x in bokeh.palettes.Colorblind8:
        colors.append(x)

    # Reactive multi choice
    multi_choice_args = dict(
        source=source,
        point_set=point_set,
        node_decompositions=node_decompositions,
        available_colors=colors,
    )
    multi_choice.js_on_change('value', CustomJS(args=multi_choice_args, code="""
        // Create new data
        const xs = [];
        const ys = [];
        const colors = [];
        const alpha = [];

        // Keep track of points already drawn
        const used_keys = new Set();

        // Plot selected node children
        const selected_node = this.value;
        const children = node_decompositions[selected_node];
        for (let i = 0; i < children.length; i++) {
            const child = children[i];
            const color = available_colors[i];
            for (const [x, y] of child) {
                const key = JSON.stringify([x, y]);
                used_keys.add(key);
                xs.push(x);
                ys.push(y);
                colors.push(color);
                alpha.push(0.9);
            }
        }

        // Plot base MIDI
        for (const [x, y] of point_set) {
            // If point has already been drawn, ignore
            const key = JSON.stringify([x, y]);
            if (used_keys.has(key))
                continue;
            xs.push(x);
            ys.push(y);
            colors.push("gray");
            alpha.push(0.25);
        }

        // Update data
        source.data = {x: xs, y: ys, color: colors, alpha: alpha};
    """))

    # Display tree
    def get_json_tree(n: TreeNode):
        node_children = list()
        for child in n.children:
            if isinstance(child, TreeNode):
                node_children.append(get_json_tree(child))
        if len(node_children) == 0:
            return n.name
        return dict(name=n.name, children=node_children)
    json_tree = json.dumps(get_json_tree(tree), indent=2)
    div = Div(text=f"<b>Here is the tree structure as JSON</b></br><pre><code>{json_tree}</code></pre>")
    layout = column(multi_choice, p, div)
    show(layout)
